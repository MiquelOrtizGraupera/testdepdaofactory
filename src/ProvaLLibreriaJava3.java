import java.util.Scanner;

public class ProvaLLibreriaJava3 {
    public static void main(String[] args) {
        DAOFactory db = DAOFactory.getDAOFactory(DAOFactory.POSTGRESQL);

        DepartamentoDAO depDAO = db.getDepartamentoDAO();

        //Crear Departamento
        Departamento dep = new Departamento(18,"COMPTABILITAT","REUS");

        //INSERTAR
        depDAO.insertarDep(dep);

        //CONSULTAR
        Departamento dep2 = depDAO.consultarDep(18);
        System.out.printf("Dep: %d, Nom: $s, Loc: %s%n",dep2.getDeptno(),dep2.getDnombre(),dep2.getLoc());

        //MODIFICAR
        dep2.setDnombre("MAFIA");
        dep2.setLoc("SOHO");
        depDAO.modificarDep(dep2);
        System.out.printf("Dep: %d, Nom: %s,Loc: %s%n",dep2.getDeptno(),dep2.getDnombre(),dep2.getLoc());

        //ELIMINAR
        depDAO.eliminarDep(18);

        //Visualitza les dades del departament que introduexies per el teclat
        Scanner sc = new Scanner(System.in);
        int entero = 1;
        while(entero > 0){
            System.out.println("Departamento: ");
            entero = sc.nextInt();
            dep = depDAO.consultarDep(entero);
            System.out.printf("Dep: %d, Nombre: %s, Loc: %s%n",dep.getDeptno(),dep.getDnombre(),dep.getLoc());
        }
    }
}
